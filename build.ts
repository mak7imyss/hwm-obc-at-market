import { build } from "esbuild";
import { header } from "./template/header";

build({
  entryPoints: ["./src/main.ts"],
  bundle: true,
  minify: false,
  target: ["ES2022"],
  platform: "browser",
  outdir: "dist",
  outbase: "src",
  banner: {
    js: `${header}`,
  },
  format: "esm",
  tsconfig: "tsconfig.json",
});
