"use strict";
exports.__esModule = true;
var esbuild_1 = require("esbuild");
var header_1 = require("./template/header");
(0, esbuild_1.build)({
  entryPoints: ["./src/main.ts"],
  bundle: true,
  minify: false,
  target: ["es2020"],
  outdir: "dist",
  outbase: "src",
  banner: {
    js: "".concat(header_1.header, "\n"),
  },
  format: "esm",
});
