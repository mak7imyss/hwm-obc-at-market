const { author, version, description, name } = require("../package.json");

export const header = `// ==UserScript==
// @name         ${name}
// @version      ${version}
// @author       ${author}
// @match        https://www.heroeswm.ru/auction.php?*&art_type=*
// @match        https://www.lordswm.com/auction.php?*&art_type=*
// @match        https://178.248.235.15/auction.php?*&art_type=*
// @description  ${description}
// @namespace    https://gitlab.com/mak7imyss/hwm-obc-at-market
// ==/UserScript==
`;
