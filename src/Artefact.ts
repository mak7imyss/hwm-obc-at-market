type artefact = {
  id?: string;
  name: string;
  durabilityActual: number;
  durabilityOverall: number;
  price: number;
  repairCost: number;
};

type logObject = {
  durability: {
    actual: number;
    overall: number;
  };
  repairsNum: number;
  battleCost: number;
  totalCost: number;
  totalDurability: number;
};

export type RepairEfficiencyFactor =
  | 0.1
  | 0.2
  | 0.3
  | 0.4
  | 0.5
  | 0.6
  | 0.7
  | 0.8
  | 0.9;

export class Artefact {
  /** актуальная прочность (x/10) */
  private durabilityActual: number;
  /** текущая прочность (10/x) */
  private durabilityOverall: number;
  /** общая стоимость */
  private totalCost: number;
  /** общая прочность */
  private totalDurability: number;
  /** начальная прочность */
  private readonly durabilityActualInit: number;
  /** начальная текущая прочность */
  private readonly durabilityOverallInit: number;
  /** начальная общая стоимость */
  private readonly totalCostInit: number;
  /** начальная общая прочность */
  private readonly totalDurabilityInit: number;
  /** id лота */
  private readonly id: string;
  /** стоимость ремонта */
  private readonly price: number;
  /** стоимость ремонта */
  private readonly repairCost: number;
  /** название предмета */
  private readonly name: string;
  /** логи изменения стоимости боя */
  private readonly battleCostLogs: logObject[];

  constructor({
    durabilityActual,
    durabilityOverall,
    id,
    name,
    price,
    repairCost,
  }: artefact) {
    this.name = name;
    this.id = id || null;
    this.durabilityActual = durabilityActual;
    this.durabilityActualInit = durabilityActual;
    this.totalDurability = durabilityActual;
    this.totalDurabilityInit = durabilityActual;
    this.durabilityOverall = durabilityOverall;
    this.durabilityOverallInit = durabilityOverall;
    this.price = price;
    this.totalCost = price;
    this.totalCostInit = price;
    this.repairCost = repairCost;
    this.battleCostLogs = [
      {
        durability: {
          actual: durabilityActual,
          overall: durabilityOverall,
        },
        repairsNum: 0,
        battleCost: this.battleCost,
        totalDurability: durabilityActual,
        totalCost: price,
      },
    ];
  }

  /**
   * Получить последние два значения стоимости боя
   */
  private get lastTwoBattleCost() {
    return this.battleCostLogs.slice(-2).map((log) => log.battleCost);
  }

  /**
   * Получить последний номер ремонта
   */
  private get lastRepairNum() {
    return this.battleCostLogs.slice(-1)[0].repairsNum;
  }

  /**
   * Получить последний лог изменения стоимости боя
   */
  private get lastBattleCostLog(): logObject {
    return this.battleCostLogs.slice(-1)[0];
  }

  /**
   * Получить стоимость боя
   */
  get battleCost() {
    return parseInt(
      (Math.round((this.totalCost / this.totalDurability) * 100) / 100).toFixed(
        2
      ),
      10
    );
  }

  /**
   * Сбросить параметры
   */
  private resetParameters() {
    this.durabilityActual = this.durabilityActualInit;
    this.durabilityOverall = this.durabilityOverallInit;
    this.totalCost = this.totalCostInit;
    this.totalDurability = this.totalDurabilityInit;
  }

  /**
   * @param repairEfficiencyFactor - коэффициент эффективности ремонта
   * @param repairCostFactor - коэффициент стоимости ремонта
   *
   * @TODO подумать над меню для указания эффективности и стоимости
   */
  private makeRepair(
    repairEfficiencyFactor: RepairEfficiencyFactor,
    repairCostFactor = 1
  ) {
    if (!this.isRepairable()) {
      console.log("Not repairable!");
      return;
    }
    this.durabilityActual = Math.floor(
      this.durabilityOverall * repairEfficiencyFactor
    );
    this.totalDurability += this.durabilityActual;
    this.durabilityOverall -= 1;
    this.totalCost += this.repairCost * repairCostFactor;

    this.battleCostLogs.push({
      durability: {
        actual: this.durabilityActual,
        overall: this.durabilityOverall,
      },
      repairsNum: this.lastRepairNum + 1,
      battleCost: this.battleCost,
      totalDurability: this.totalDurability,
      totalCost: this.totalCost,
    });
  }

  /**
   * @param repairEfficiency - коэффициент эффективности ремонта
   * @param repairCostFactor - коэффициент стоимости ремонта
   */
  makeOptimalRepair(
    repairEfficiency: RepairEfficiencyFactor,
    repairCostFactor = 1
  ) {
    while (this.isRepairable()) {
      this.makeRepair(repairEfficiency, repairCostFactor);
      if (this.lastTwoBattleCost[0] < this.lastTwoBattleCost[1]) {
        this.battleCostLogs.pop();
        break;
      }
    }
    this.resetParameters();
  }

  /**
   * @param repairEfficiency - коэффициент эффективности ремонта
   * @param repairCostFactor - коэффициент стоимости ремонта
   */
  makeFullRepair(
    repairEfficiency: RepairEfficiencyFactor,
    repairCostFactor = 1
  ) {
    while (this.isRepairable()) {
      this.makeRepair(repairEfficiency, repairCostFactor);
    }
    this.resetParameters();
  }

  /**
   * Проверка ремонтопригодности
   */
  isRepairable(): boolean {
    if (this.durabilityOverall === 1) {
      return false;
    }
    return true;
  }

  /**
   * Вывести информацию о предмете
   */
  showInfo() {
    console.log(
      `durabilityActual: ${
        this.lastBattleCostLog.durability.actual
      }\ndurabilityOverall: ${
        this.lastBattleCostLog.durability.overall
      }\ntotalCost: ${this.lastBattleCostLog.totalCost}\ntotalDurability: ${
        this.lastBattleCostLog.totalDurability
      }\nbattleСost: ${
        this.lastBattleCostLog.battleCost
      }\nbattleCostLog: ${JSON.stringify(this.battleCostLogs, null, 2)}\n    `
    );
  }

  /**
   * Вывести информацию о предмете в виде html
   */
  get divInfo() {
    if (Number.isNaN(this.lastBattleCostLog.battleCost)) return "";
    return `<div class="optimalScrapping">
              <b title="Количество проведенных ремонтов: ${this.lastBattleCostLog.repairsNum} Общее количество боёв: ${this.lastBattleCostLog.totalDurability} Общие затраты: ${this.lastBattleCostLog.totalCost}">[?]</b> 
              <b class="durability" title="Оптимальная прочность">0/${this.lastBattleCostLog.durability.overall}</b> |
              <b class="cost" title="Оптимальная стоимость боя" style="background-color: #7fffd4" alt="${this.lastBattleCostLog.battleCost}">${this.lastBattleCostLog.battleCost}</b>
            </div>`;
  }
}
