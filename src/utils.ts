import type { RepairEfficiencyFactor } from "./Artefact";
import { Artefact } from "./Artefact";

const hostService = "daily.heroeswm.ru";
const hostProxy = "corsproxy.io";

/**
 * Получить стоимость ремонта лота
 * @param lotId - id лота
 */
export const getRepairCost = async (lotId: string): Promise<number> => {
  const headers = new Headers([["Host", hostService]]);

  return fetch(
    `https://${hostProxy}/?${encodeURIComponent(
      `https://${hostService}/services/help/auction_lot_protocol.php?id=${lotId}`
    )}`,
    { headers }
  )
    .then((response) => response.text())
    .then((result) => parseInt(result.split("|")[3], 10))
    .catch((error) => {
      console.error("error:\n", error);
      throw new Error(
        "Не удалось получить стоимость ремонта лота. Вероятно проблема c прокси сервером."
      );
    });
};

export const repairing = async () => {
  const auctionLots = document.querySelectorAll("tr.wb");

  const lotId = auctionLots[0]
    .querySelector("td[valign] a")
    .getAttribute("name");
  // console.debug("lotId:", lotId);
  const name = auctionLots[0]
    .querySelector(".shop_art_info a")
    .getAttribute("name");
  const id = document.URL.match(/art_type=(\w+)/)[1];

  // получаем стоимость ремонта
  const repairCost = await getRepairCost(lotId);
  for (const lot of auctionLots) {
    const price = parseInt(
      lot
        .querySelector('div[id^="au"] td:last-child')
        .innerText.replace(",", ""),
      10
    );
    const durability = lot
      .querySelector(".art_durability_hidden")
      .innerText.split("/")
      .map((strength: string) => parseInt(strength, 10));

    const art = new Artefact({
      id,
      name,
      price,
      durabilityActual: durability[0],
      durabilityOverall: durability[1],
      repairCost,
    });

    // TODO подумать над возможностью указания процента ремонта на странице
    const repairEfficiency = (
      document.querySelector("#obcRepairEfficiency") as HTMLInputElement
    ).value;
    const repairCostFactor = (
      document.querySelector("#obcRepairCost") as HTMLInputElement
    ).value;

    art.makeOptimalRepair(
      parseFloat(repairEfficiency) as RepairEfficiencyFactor,
      parseInt(repairCostFactor, 10) / 100
    );
    const divWithCalculations = art.divInfo;
    const descriptionCell = lot.querySelector("td[valign='top']");
    if (descriptionCell.querySelector("div.optimalScrapping")) {
      descriptionCell.querySelector("div.optimalScrapping").outerHTML =
        divWithCalculations;
    } else {
      descriptionCell.innerHTML = descriptionCell.innerHTML.replace(
        "Прочность",
        `${divWithCalculations}\nПрочность`
      );
    }
  }

  const listCosts = document.querySelectorAll(".cost");

  const costsArray = Array.from(listCosts).map((cost: HTMLElement) =>
    parseFloat(cost.innerText)
  );
  const minCost = Math.min(...costsArray);
  document.querySelectorAll(`.cost[alt='${minCost}']`).forEach(
    (element: HTMLElement) =>
      // eslint-disable-next-line no-param-reassign
      (element.style.backgroundColor = "yellow")
  );
};

const toggleSettings = () => {
  const settingsMenu = document.getElementById("settingsMenu");
  settingsMenu.style.display =
    settingsMenu.style.display === "none" || settingsMenu.style.display === ""
      ? "block"
      : "none";
};

export const setOptimalBreak = () => {
  // Создаем контейнер для кнопки и меню
  const container = document.createElement("div");
  container.style.setProperty("display", "inline-block");

  // Создаем кнопку
  const toggleSettingsButton = document.createElement("button");
  toggleSettingsButton.id = "toggleSettings";
  toggleSettingsButton.textContent = "Значения оптимального слома";
  toggleSettingsButton.addEventListener("click", toggleSettings);

  // Создаем меню
  const settingsMenu = document.createElement("div");
  settingsMenu.id = "settingsMenu";
  settingsMenu.style.setProperty("display", "none");
  settingsMenu.style.setProperty("background-color", "#ffffff");
  settingsMenu.style.setProperty("padding", "7px");
  settingsMenu.style.setProperty("border-radius", "5px");
  settingsMenu.style.setProperty("box-shadow", "0 0 10px rgba(0, 0, 0, 0.1)");
  settingsMenu.style.setProperty("position", "relative");
  settingsMenu.style.setProperty("z-index", "2");
  settingsMenu.style.setProperty("height", "100%");

  // Создаем форму
  const form = document.createElement("form");
  form.name = "obc";
  form.style.display = "inline";

  // Создаем и добавляем селект
  const select = document.createElement("select");
  select.id = "obcRepairEfficiency";

  const values = [
    "90%",
    "80%",
    "70%",
    "60%",
    "50%",
    "40%",
    "30%",
    "20%",
    "10%",
  ];

  values.forEach((value) => {
    const option = document.createElement("option");
    option.value = (parseFloat(value) / 100).toString();
    option.textContent = value;
    select.appendChild(option);
  });

  // Создаем и добавляем инпут
  const input = document.createElement("input");
  input.type = "number";
  input.id = "obcRepairCost";
  input.value = "100";
  input.min = "0";
  input.max = "200";
  input.step = "1";
  input.style.width = "60px";

  // Добавляем селект и инпут в форму
  form.appendChild(select);
  form.appendChild(input);

  // Добавляем форму в меню
  settingsMenu.appendChild(form);

  // Добавляем обработчик событий
  input.addEventListener("input", async () => {
    await repairing();
  });
  select.addEventListener("change", async () => {
    await repairing();
  });

  // Добавляем кнопку и меню в контейнер
  container.appendChild(toggleSettingsButton);
  container.appendChild(settingsMenu);

  // Вставляем контейнер в нужное место на странице
  const targetElement = document.querySelector(
    "td[colspan='4'][align='right'][valign='top']"
  );
  targetElement.innerHTML += "<br>";
  targetElement.appendChild(container);
};

`<td><a name="124264512"></a><table><tbody><tr><td width="50" height="50"><div><div class="arts_info shop_art_info" style="height: 50px; width: 50px;" onmouseover="hwm_mobile_show_arts_durability(true, true);" onmouseout="hwm_mobile_hide_arts_durability()"><div class="art_durability_hidden" style="display:none;">1/25</div><a href="art_info.php?id=mirror" name="mirror"><img src="https://dcdn1.heroeswm.ru/i/art_fon_100x100.png" border="0" width="50px" height="50px" "="" alt="" class="cre_mon_image1"><img src="https://dcdn2.heroeswm.ru/i/artifacts/events/mirror.png" border="0" width="50px" height="50px" hint="Зеркало перемен <br /> <br />Прочность: 1/25" alt="" class="cre_mon_image2 show_hint" hwm_hint_added="1"></a></div></div></td><td valign="top">Зеркало перемен&nbsp;<b><a class="pi" href="auction_lot_protocol.php?id=124264512">[i]</a></b><br><div class="optimalScrapping">
              <b title="Количество проведенных ремонтов: 4 Общее количество боёв: 83 Общие затраты: 72000">[?]</b>
              <b class="durability" title="Оптимальная прочность">0/21</b> |
              <b class="cost" title="Оптимальная стоимость боя" style="background-color: #7fffd4" alt="867">867</b>
            </div>
Прочность: <font color="red"><b>1</b></font>/<font color="red"><b>25</b></font></td></tr></tbody></table></td><td align="center"><b>Купить сразу!</b></td><td align="left"><table cellspacing="0" cellpadding="0"><tbody><tr><td><div id="au124264512" style="display: inline;"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr><td><img width="24" height="24" src="https://dcdn2.heroeswm.ru/i/r/48/gold.png?v=3.23de65" border="0" title="Золото" alt="" class="rs"></td><td>8,000</td></tr></tbody></table></div></td><td>&nbsp;</td></tr></tbody></table></td><td>2 д. 12 ч. 33 мин. </td><td valign="top"><form name="fr124264512" action="auction_buy_now.php" method="post" style="display: inline;"><a class="pi" href="pl_info.php?id=6805212"><b>пенёк великий 3</b></a><br><input type="hidden" name="lotid" value="124264512"><input type="hidden" name="mid" value="21"><input type="hidden" name="msign" value="f4a7b655fd89741def6ecb9baa0541e2"><input type="hidden" name="cat" value="other"><input type="hidden" name="sort" value="0"><input type="hidden" name="art_type" value="mirror"><input type="hidden" name="type" value="0"><input type="hidden" name="gid" value="a5e2c030e520b1ce0322a3d3e56b0e2b"><input type="hidden" name="marker" value="2840840"><input type="hidden" name="buy_num" id="buy_num124264512" value="0"><input type="hidden" name="buy_num_x" id="buy_num_x124264512" value="-1"><input type="hidden" name="buy_num_y" id="buy_num_y124264512" value="-1"><input type="hidden" name="down" id="down124264512" value="-1"><input type="hidden" name="up" id="up124264512" value="-1"><div id="swf_but124264512"><a href="#" onclick="javascript: show_js_button(124264512, 1, ' disabled ');return false;">Купить &gt;&gt;</a></div></form></td>`;
