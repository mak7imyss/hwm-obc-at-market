import { repairing, setOptimalBreak } from "./utils";

(async function () {
  if (!document.URL.includes("art_type")) return;

  setOptimalBreak();

  await repairing();
})();
