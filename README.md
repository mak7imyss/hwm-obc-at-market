### Heroeswm Optimal Battle Cost

Скрипт для отображения на странице рынка оптимального слома у лотов.  
В расчет берется стоимость ремонта из коэффициента 1 и эффективности 0.9.

#### Install

1. clone repository
2. go to folder
3. execute in `npm install`
4. execute in `npm run build`
5. copy code from `./dist/main.js` to new script for Tampermonkey
